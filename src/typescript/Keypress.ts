export class Keypress {
    private element: HTMLElement | Document
    constructor(element: HTMLElement | Document) {
        this.element = element
    }
    handle(key: string, fn: () => void): void {
        this.element.onkeyup = (event: KeyboardEvent) => {
            if(event.key === key) {
                fn()
            }
        }
    }
    esc(fn: () => void): void {
        this.handle('Escape', fn)
    }
}