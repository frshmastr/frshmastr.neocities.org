export type BaseLayoutMetadata = {
    title: string
    description: string
    image: string
}