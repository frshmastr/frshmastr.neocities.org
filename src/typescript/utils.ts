export function sortPostByDate(posts: any[]): any[] {
    return posts.sort((a, b) => {
        let dateA = new Date(a.frontmatter.date).getTime()
        let dateB = new Date(b.frontmatter.date).getTime()
        return dateB - dateA
    })
}