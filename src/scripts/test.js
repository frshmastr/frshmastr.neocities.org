/*const child_process = require('node:child_process')
const image = '../../public/img/gallery/thumbnails/80s_workout.webp'
const test = child_process.execSync(`identify ${image}`)
const size = test.toString().split(' ').splice(2, 1)[0].split('x')

const {writeFileSync} =  require('fs')
const path = require('node:path')
const info = {
    file: path.basename(image),
    width: size[0],
    height: size[1]
}
const json = JSON.stringify(info, null, 4)
if(json) {
    writeFileSync('prueba', json)
}*/
const path = require('node:path')
const { execSync, exec } = require('node:child_process')
const fs = require('fs')
const gallery = 'public/gallery/'
const src = gallery + 'lossless/'
const fullsizeAssets = 'public/gallery/fullsize/'
const thumbnailAssets = 'public/gallery/thumbnails/'
const assest = {
    lossless: gallery + 'lossless/',
    fullsize: gallery + 'fullsize/',
    thumbnails: gallery + 'thumbnails/'
}
const makeFullSize = (input) => {
    return `magick convert ${input}`
}
async function processImages(command, path) {
    const files = await fs.promises.opendir(src)
    for await (const dirent of files) {
        const input = src + dirent.name
        const ext = path.extname(input)
        const basename = path.basename(input, ext)
        const output = basename + '.webp'
        exec(`magick convert ${input} ${path + output}`,
            (error, stdout, stderr) => {
                if(error) {
                    console.log(error.message)
                    return
                }
                if(stderr) {
                    console.log(stderr)
                    return
                }
                console.log(`fullsize ${output}: ok`)
            })
    }
}